using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Singleton
    public static AudioManager instance;
    #endregion
    #region Variables
    [SerializeField]
    private GridManager gm;
    [SerializeField]
    private AudioSource music;
    [SerializeField]
    private AudioSource selectBlock;
    [SerializeField]
    private AudioSource deselectBlock;
    [SerializeField]
    private AudioSource positiveFeedback;
    [SerializeField]
    private AudioSource negativeFeedback;
    [SerializeField]
    private AudioSource click;
    #endregion
    #region Properties
    public AudioSource Music { get { return music; } set { music = value; } }
    public AudioSource SelectBlock { get { return selectBlock; } set { selectBlock = value; } }
    public AudioSource DeselectBlock { get { return deselectBlock; } set { deselectBlock = value; } }
    public AudioSource PositiveFeedback { get { return positiveFeedback; } set { positiveFeedback = value; } }
    public AudioSource NegativeFeedback { get { return negativeFeedback; } set { negativeFeedback = value; } }
    public AudioSource Click { get { return click; } set { click = value; } }
    #endregion
    private void Awake()
    {
        if (instance != null) Destroy(this);
        else instance = this;
        gm = GridManager.instance;
        PlayMusic();
    }

    private void Update()
    {
        if(gm.NumberOfTotalTurns / 4 >= gm.NumberOfTurns)
        {
            music.pitch = 1.06f;
        }
        else
        {
            music.pitch = 1f;
        }
    }
    private void PlayMusic()
    {
        music.Play();
    }

    public void PlayClickSound()
    {
        click.Play();
    }
}
