using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Block : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private int type;

    [SerializeField]
    private int row;

    [SerializeField]
    private int col;

    private GridManager gm = GridManager.instance;

    [SerializeField]
    private List<GameObject> combinationUp = new List<GameObject>();
    [SerializeField]
    private List<GameObject> combinationDown = new List<GameObject>();
    [SerializeField]
    private List<GameObject> combinationLeft = new List<GameObject>();
    [SerializeField]
    private List<GameObject> combinationRight = new List<GameObject>();

    [SerializeField]
    private bool fellOrRefill = false;
    #endregion
    #region Properties    
    public int Type { get { return type; } set{ type = value; } } 
    public int Row { get { return row; } set { row = value; } }
    public int Col { get { return col; } set { col = value; } }
    public List<GameObject> CombinationUp { get { return combinationUp; } }
    public List<GameObject> CombinationDown { get { return combinationDown; } }
    public List<GameObject> CombinationLeft { get { return combinationLeft; } }
    public List<GameObject> CombinationRight { get { return combinationRight; } }
    
    public bool FellOrRefill { get { return fellOrRefill; } set { fellOrRefill = value; } }
    #endregion

    public void CheckForMoves(int _minimumBlocks, int _rows, int _cols)
    {        
        if (combinationUp.Count > 0) combinationUp.Clear();
        if (combinationDown.Count > 0) combinationDown.Clear();
        if (combinationLeft.Count > 0) combinationLeft.Clear();
        if (combinationRight.Count > 0) combinationRight.Clear();
        //Can match Up
        for (int i = row + 1; i < _rows; i++)
        {
            if(gm.Blocks[i, col] != null)
            {
                if (gm.Blocks[i, col].GetComponent<Block>().type == type && !combinationUp.Contains(gm.Blocks[i, col]))
                {
                    combinationUp.Add(gm.Blocks[i, col]);
                }
                else
                {
                    break;
                }
            }            
        }

        //Can match Down
        for (int i = row - 1; i >= 0; i--)
        {
            if (gm.Blocks[i, col] != null)
            {
                if (gm.Blocks[i, col].GetComponent<Block>().type == type && !combinationDown.Contains(gm.Blocks[i, col]))
                {
                    combinationDown.Add(gm.Blocks[i, col]);
                }
                else
                {
                    break;
                }
            }            
        }

        //Can match Left
        for (int i = col - 1; i >= 0; i--)
        {
            if (gm.Blocks[row, i] != null)
            {
                if (gm.Blocks[row, i].GetComponent<Block>().type == type && !combinationLeft.Contains(gm.Blocks[row, i]))
                {
                    combinationLeft.Add(gm.Blocks[row, i]);
                }
                else
                {
                    break;
                }
            }            
        }

        //Can match Right
        for (int i = col + 1; i < _cols; i++)
        {
            if (gm.Blocks[row, i] != null)
            {
                if (gm.Blocks[row, i].GetComponent<Block>().type == type && !combinationRight.Contains(gm.Blocks[row, i]))
                {
                    combinationRight.Add(gm.Blocks[row, i]);
                }
                else
                {
                    break;
                }
            }            
        }

        if (combinationUp.Count >= _minimumBlocks || combinationDown.Count >= _minimumBlocks || combinationLeft.Count >= _minimumBlocks || combinationRight.Count >= _minimumBlocks && !gm.PossibleMoves)
        {
            gm.PossibleMoves = true;
        }

        int combCount = 0;
        combCount = combinationUp.Count + combinationDown.Count + combinationLeft.Count + combinationRight.Count;
        if (combCount >= _minimumBlocks) gm.PossibleMoves = true;
    }

    //public void CheckForCombos(int _minimumBlocks, int _rows, int _cols)
    //{
    //    //Can match Up
    //    for (int i = row + 1; i < _rows; i++)
    //    {
    //        if (gm.Blocks[i, col] != null)
    //        {
    //            if (gm.Blocks[i, col].GetComponent<Block>().type == type && !combinationUp.Contains(gm.Blocks[i, col]))
    //            {
    //                combinationUp.Add(gm.Blocks[i, col]);
    //            }
    //            else
    //            {
    //                break;
    //            }
    //        }
    //    }

    //    //Can match Down
    //    for (int i = row - 1; i >= 0; i--)
    //    {
    //        if (gm.Blocks[i, col] != null)
    //        {
    //            if (gm.Blocks[i, col].GetComponent<Block>().type == type && !combinationDown.Contains(gm.Blocks[i, col]))
    //            {
    //                combinationDown.Add(gm.Blocks[i, col]);
    //            }
    //            else
    //            {
    //                break;
    //            }
    //        }
    //    }

    //    //Can match Left
    //    for (int i = col - 1; i >= 0; i--)
    //    {
    //        if (gm.Blocks[row, i] != null)
    //        {
    //            if (gm.Blocks[row, i].GetComponent<Block>().type == type && !combinationLeft.Contains(gm.Blocks[row, i]))
    //            {
    //                combinationLeft.Add(gm.Blocks[row, i]);
    //            }
    //            else
    //            {
    //                break;
    //            }
    //        }
    //    }

    //    //Can match Right
    //    for (int i = col + 1; i < _cols; i++)
    //    {
    //        if (gm.Blocks[row, i] != null)
    //        {
    //            if (gm.Blocks[row, i].GetComponent<Block>().type == type && !combinationRight.Contains(gm.Blocks[row, i]))
    //            {
    //                combinationRight.Add(gm.Blocks[row, i]);
    //            }
    //            else
    //            {
    //                break;
    //            }
    //        }
    //    }

    //    int combCount = 0;
    //    combCount = combinationUp.Count + combinationDown.Count + combinationLeft.Count + combinationRight.Count;
    //    if (combCount > 1)
    //    {
    //        foreach (GameObject go in combinationUp)
    //        {
    //            gm.Blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
    //            Destroy(go);
    //        }
    //        foreach (GameObject go in combinationDown)
    //        {
    //            gm.Blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
    //            Destroy(go);
    //        }
    //        foreach (GameObject go in combinationLeft)
    //        {
    //            gm.Blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
    //            Destroy(go);
    //        }
    //        foreach (GameObject go in combinationRight)
    //        {
    //            gm.Blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
    //            Destroy(go);
    //        }
    //        Destroy(gameObject);
    //        gm.Blocks[row, col] = null;
    //    }
    //}
}
