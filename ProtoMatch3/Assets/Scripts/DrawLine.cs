using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLine : MonoBehaviour
{
	#region Variables
	[SerializeField]
	private GameObject linePrefab;
	private GameObject currentLine;
	private LineRenderer lineRenderer;
	private List<Vector2> fingerPositions = new List<Vector2>();
	private GridManager gm;
	[SerializeField]
	private List<GameObject> blocks;
	#endregion
	private void Awake()
	{
		gm = GridManager.instance;
		blocks = new List<GameObject>();
	}
	void Update()
	{
		if (gm.STATE == GridManager.GAME_STATE.WAITING || gm.NumberOfTurns <= 0)
		{
			DestroyLine();
			return;
		}

		if (Input.GetMouseButtonDown(0))
		{
			CreateLine();
		}
		if (Input.GetMouseButton(0))
		{
			UpdateLine();
		}
		if(Input.GetMouseButtonUp(0))
		{
			DestroyLine();
		}
	}

	void CreateLine()
	{		
		fingerPositions.Clear();
		blocks.Clear();
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

		if(hit.collider != null && hit.collider.GetComponent<Block>() && blocks.Count == 0)
        {
			blocks.Add(hit.collider.gameObject);
			currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
			lineRenderer = currentLine.GetComponent<LineRenderer>();
			fingerPositions.Add(hit.transform.position);
			fingerPositions.Add(hit.transform.position);
			lineRenderer.SetPosition(0, fingerPositions[0]);
			lineRenderer.SetPosition(1, fingerPositions[1]);
		}		
	}

	void UpdateLine()
	{
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

		if (hit.collider != null && hit.collider.GetComponent<Block>() && blocks.Count > 0 && !blocks.Contains(hit.collider.gameObject) && lineRenderer != null)
		{
			blocks.Add(hit.collider.gameObject);
			fingerPositions.Add(hit.collider.transform.position);
			lineRenderer.positionCount++;
			lineRenderer.SetPosition(lineRenderer.positionCount - 1, hit.collider.transform.position);
		}		
	}

	public void DestroyLine()
	{
		for(int i = 0; i < blocks.Count; i++)
        {
			blocks[i] = null;
        }
		Destroy(currentLine);
	}

	public void DestroyLastPoint()
    {
		lineRenderer.positionCount--;
		blocks.RemoveAt(blocks.Count - 1);
	}
}

