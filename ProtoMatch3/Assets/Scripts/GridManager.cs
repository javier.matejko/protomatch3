using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    #region Singleton
    public static GridManager instance;
    #endregion
    #region Variables
    [SerializeField]
    private int rows = 5;
    [SerializeField]
    private int cols = 8;
    [SerializeField]
    private float tileSize = 1f;

    [SerializeField]
    private int amountOfTypesInGame = 0;
    [SerializeField]
    private GameObject[] referenceTiles;

    [SerializeField]
    private int numberOfTotalTurns = 15;
    [SerializeField]
    private int numberOfCurrentTurns = 15;

    [SerializeField]
    private bool isRestarting = false;

    [SerializeField]
    private int points = 0;
    [SerializeField]
    private int pointsPerBlock = 25;

    [SerializeField]
    private GameObject[,] blocks;
    [SerializeField]
    private bool possibleMoves = false;
    [SerializeField]
    private int minimumBlocks = 1;
    private Vector2 tempPosition = new Vector2();
    public enum GAME_STATE { WAITING, PLAYING }
    public GAME_STATE STATE = GAME_STATE.WAITING;
    #endregion

    #region Properties
    public bool PossibleMoves { get { return possibleMoves; } set { possibleMoves = value; } }
    public GameObject[,] Blocks { get { return blocks; } set { blocks = value; } }

    public int MinimumBlocks { get { return minimumBlocks; } }
    public int NumberOfTurns { get { return numberOfCurrentTurns;  } set { numberOfCurrentTurns = value; } }
    public int NumberOfTotalTurns { get { return numberOfTotalTurns; }  }
    public int Points { get { return points; } set { points = value; } }
    public int PointsPerBlock { get { return pointsPerBlock; } }
    public bool IsRestarting { get { return isRestarting; } }
    #endregion

    private void Awake()
    {
        if (instance != null) Destroy(this);
        else instance = this;
        if (amountOfTypesInGame > referenceTiles.Length) amountOfTypesInGame = referenceTiles.Length;
        numberOfCurrentTurns = numberOfTotalTurns;
    }
    private void Start()
    {
        GenerateGrid();
    }

    private void GenerateGrid()
    {
        blocks = new GameObject[rows, cols];

        for (int row = 0; row < rows; row++)
        {
            for(int col = 0; col < cols; col++)
            {
                int rand = UnityEngine.Random.Range(0, amountOfTypesInGame);
                GameObject tile = (GameObject)Instantiate(referenceTiles[rand], transform);

                float posX = row * tileSize;
                float posY = col * -tileSize;

                tile.transform.localPosition = new Vector2(col, row);

                blocks[row, col] = tile;

                blocks[row, col].GetComponent<Block>().Type = rand;
                blocks[row, col].GetComponent<Block>().Row = row;
                blocks[row, col].GetComponent<Block>().Col = col;
                blocks[row, col].gameObject.name = "(" + row + ", " + col+ ")";
            }
        }

        float gridW = cols * tileSize;
        float gridH = rows * tileSize;

        transform.position = new Vector2(-gridW / 2 + tileSize / 2, -gridH / 2 + tileSize / 2);

        StartCoroutine(ICheckForPossibleMoves());
    }

    private void CheckForPossibleMoves()
    {
        possibleMoves = false;        

        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                if(blocks[row, col] != null) blocks[row, col].GetComponent<Block>().CheckForMoves(minimumBlocks, rows, cols);
            }
        }        

        if(!possibleMoves)
        {
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Destroy(blocks[row, col].gameObject);
                }
            }
            GenerateGrid();
        }
        else
        { 
            STATE = GAME_STATE.PLAYING; 
        }
    }

    private IEnumerator ICheckForPossibleMoves()
    {
        CheckForPossibleMoves();
        yield return new WaitForSeconds(0.4f);
    }

    public void LowerRows()
    {
        int nullCount = 0;

        for (int col = 0; col < cols; col++)
        {
            for (int row = 0; row < rows; row++)
            {
                if (blocks[row, col] == null)
                {
                    nullCount++;
                }
                else if (nullCount > 0)
                {
                    Block bl = blocks[row, col].GetComponent<Block>();
                    bl.Row -= nullCount;
                    tempPosition = new Vector2(bl.Col, bl.Row);//the row and column are in reverse
                    blocks[row, col].transform.localPosition = tempPosition;
                    bl.FellOrRefill = true;
                    blocks[row - nullCount, col] = bl.gameObject;
                    blocks[row - nullCount, col].gameObject.name = "(" + bl.Row + ", " + bl.Col + ")";
                    blocks[row, col] = null;

                    //if(blocks[row - nullCount, col].GetComponent<Block>() == null)
                    //{
                    //    var type = Type.GetType("Block");

                    //    blocks[row - nullCount, col].gameObject.AddComponent(type);
                    //}
                    
                }
            }
            nullCount = 0;
        }

        RefillGrid();
        StartCoroutine(ICheckForPossibleMoves());
    }

    private void RefillGrid()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                if(blocks[row, col] == null)
                {
                    int rand = UnityEngine.Random.Range(0, amountOfTypesInGame);
                    GameObject tile = (GameObject)Instantiate(referenceTiles[rand], transform);

                    tile.transform.localPosition = new Vector2(col, row);

                    blocks[row, col] = tile;

                    blocks[row, col].GetComponent<Block>().Type = rand;
                    blocks[row, col].GetComponent<Block>().Row = row;
                    blocks[row, col].GetComponent<Block>().Col = col;
                    blocks[row, col].GetComponent<Block>().FellOrRefill = true;
                    blocks[row, col].gameObject.name = "(" + row + ", " + col + ")";
                }                
            }
        }
    }

    public void RestartGame()
    {
        StartCoroutine(IRestartGame());
    }
    private IEnumerator IRestartGame()
    {
        isRestarting = true;
        yield return new WaitForSeconds(2f);
        ClearBoard();
        GenerateGrid();
        points = 0;
        numberOfCurrentTurns = numberOfTotalTurns;
        isRestarting = false;
    }
    private void ClearBoard()
    {
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                Destroy(blocks[row, col]);                
            }
        }
    }

    public bool CheckForCombos()
    {
        bool hadCombo = false;
        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                Block bl = blocks[row, col].GetComponent<Block>();
                if(bl.FellOrRefill)
                {
                    int comb = bl.CombinationUp.Count + bl.CombinationDown.Count + bl.CombinationLeft.Count + bl.CombinationRight.Count;
                    if(comb >= minimumBlocks)
                    {
                        foreach(GameObject go in bl.CombinationUp)
                        {
                            blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
                            Destroy(go);
                        }
                        foreach (GameObject go in bl.CombinationDown)
                        {
                            blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
                            Destroy(go);
                        }
                        foreach (GameObject go in bl.CombinationLeft)
                        {
                            blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
                            Destroy(go);
                        }
                        foreach (GameObject go in bl.CombinationRight)
                        {
                            blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
                            Destroy(go);
                        }
                        Destroy(bl);
                        blocks[row, col] = null;
                        hadCombo = true;
                        return hadCombo;
                    }
                }
            }
        }

        return hadCombo;
    }
}
