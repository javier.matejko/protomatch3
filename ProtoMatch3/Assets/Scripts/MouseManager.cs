using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    #region Variables
    [SerializeField]
    private List<GameObject> selectedBlocks = new List<GameObject>();
    private GridManager gm;
    private AudioManager am;
    private DrawLine dl;
    #endregion

    private void Awake()
    {
        gm = GridManager.instance;
        am = AudioManager.instance;
        dl = this.GetComponent<DrawLine>();
    }
    private void Update()
    {
        if (gm.STATE == GridManager.GAME_STATE.WAITING || gm.NumberOfTurns <= 0) return;
        if(Input.GetMouseButtonDown(0) && selectedBlocks.Count == 0)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null && hit.collider.GetComponent<Block>())
            {
                am.SelectBlock.Play();
                selectedBlocks.Add(hit.collider.gameObject);
            }
        }

        if (Input.GetMouseButton(0) && selectedBlocks.Count > 0)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            
            if (hit.collider != null && hit.collider.GetComponent<Block>() && ContainsCombination(hit.collider) && !selectedBlocks.Contains(hit.collider.gameObject))
            {
                am.SelectBlock.Play();
                selectedBlocks.Add(hit.collider.gameObject);                
            }

            

            if (hit.collider != null && hit.collider.GetComponent<Block>() && !selectedBlocks.Contains(hit.collider.gameObject))
            {
                if(selectedBlocks.Count > gm.MinimumBlocks)
                {
                    DestroyBlocks();
                }
                else
                {
                    selectedBlocks.Clear();
                    am.NegativeFeedback.Play();
                    dl.DestroyLine();
                }                
            }
            
            try
            {
                if (selectedBlocks.Count > 1 && hit.collider.gameObject == selectedBlocks[selectedBlocks.Count - 2])
                {
                    selectedBlocks.RemoveAt(selectedBlocks.Count - 1);
                    am.DeselectBlock.Play();
                    dl.DestroyLastPoint();
                }
            }
            catch
            {

            }          
                      
            
        }

        if (Input.GetMouseButtonUp(0) && selectedBlocks.Count > 0)
        {
            DestroyBlocks();
        }
    }

    private bool ContainsCombination(Collider2D collider)
    {
        bool combines = false;

        //combination for adjacent (horizontal or vertical) blocks
        if (selectedBlocks[selectedBlocks.Count - 1].GetComponent<Block>().CombinationUp.Contains(collider.gameObject) ||
            selectedBlocks[selectedBlocks.Count - 1].GetComponent<Block>().CombinationDown.Contains(collider.gameObject) ||
            selectedBlocks[selectedBlocks.Count - 1].GetComponent<Block>().CombinationLeft.Contains(collider.gameObject) ||
            selectedBlocks[selectedBlocks.Count - 1].GetComponent<Block>().CombinationRight.Contains(collider.gameObject) &&
            !selectedBlocks.Contains(collider.gameObject))
        {
            combines = true;
        }

        return combines;
    }
    private void DestroyBlocks()
    {
        dl.DestroyLine();
        //destroys blocks if they are above minimum of blocks
        if (selectedBlocks.Count >= gm.MinimumBlocks + 1)
        {
            gm.STATE = GridManager.GAME_STATE.WAITING;
            am.PositiveFeedback.Play();
            foreach (GameObject go in selectedBlocks)
            {
                gm.Blocks[go.GetComponent<Block>().Row, go.GetComponent<Block>().Col] = null;
                Destroy(go);
            }
            ApplyPoints();
            selectedBlocks.Clear();
            gm.LowerRows();
            gm.NumberOfTurns--;

            //while(gm.CheckForCombos())
            //{
            //    gm.LowerRows();
            //}

            if(gm.NumberOfTurns <= 0)
            {
                gm.RestartGame();
            }
        }
        else
        {
            selectedBlocks.Clear();
            am.DeselectBlock.Play();
        }
    }

    private void ApplyPoints()
    {
        gm.Points += selectedBlocks.Count * gm.PointsPerBlock;
    }
}
