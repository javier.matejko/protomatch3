using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class UI_GridManager : MonoBehaviour
{
    [SerializeField]
    private GridManager gm;
    [SerializeField]
    private TextMeshProUGUI turnsText;
    [SerializeField]
    private TextMeshProUGUI pointsText;
    [SerializeField]
    private GameObject restartingMessage;

    private void Awake()
    {
        gm = GridManager.instance;
    }

    private void Update()
    {        
        if (gm == null) return;
        turnsText.text = "Turns: " + gm.NumberOfTurns.ToString();
        pointsText.text = "Your score: " + gm.Points.ToString();
        if (gm.IsRestarting && !restartingMessage.activeSelf) restartingMessage.SetActive(true);
        if (!gm.IsRestarting && restartingMessage.activeSelf) restartingMessage.SetActive(false);
    }

    public void RestartGame()
    {
        gm.RestartGame();
    }
}
